## Please use the ordering of taget for developing
APP_NAME=myAPI
TEST_REPORT=testreport
export GO111MODULE:=on
## for powershell of windows
# $Env:GO111MODULE="on"


.PHONY: install-tools
install-tools:
	go get -u -v github.com/swaggo/swag/cmd/swag
	go get -u -v github.com/alecthomas/template
	go get -u -v github.com/gin-gonic/gin
	go get -u -v github.com/swaggo/files
	go get -u -v github.com/swaggo/swag
	go get -u -v github.com/swaggo/gin-swagger
	go get -u -v github.com/gofrs/uuid
	

# Preparing the developing enviroment	
.PHONY: init
init:
	mkdir bin
	$(MAKE) install-tools
#	go get -d ./...
	go mod tidy
	$(MAKE) doc


# Producing the swagger document of APIs
.PHONY: doc
doc:
	swag init  --parseInternal=true
# 	swag init -d ./ -g main.go --parseInternal=true

# Building the execuable file
.PHONY: build
build:
	go build -o ./bin/$(APP_NAME) .

# Running the application
.PHONY: run
run:
	./bin/$(APP_NAME)

# Running all unit tests
.PHONY: test-all
test-all:
	go test -v ./...

# Running not passed unit tests
.PHONY: test
test:
	go test  ./...


# View the coverage of unit test
.PHONY: coverage
coverage:
	go test ./... -v -cover -coverprofile=$(TEST_REPORT)
	go tool cover -html=$(TEST_REPORT)

.PHONY: clean
clean:
	rm ./bin/$(APP_NAME)
	rm  $(TEST_REPORT)
