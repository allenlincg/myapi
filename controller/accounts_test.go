package controller

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"myAPI/httputility"
	"myAPI/model"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/gin-gonic/gin"
)

type accountCase struct {
	method string
	url    string
	status int
	want   interface{} //model.Account
}

func StructToMap(obj interface{}) (newMap map[string]interface{}, err error) {
	data, err := json.Marshal(obj) // Convert to a json string

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &newMap) // Convert to a map
	return
}

func SliceToMaps(objs []interface{}) (newMaps []map[string]interface{}, err error) {
	for obj := range objs {
		if newMap, err := StructToMap(obj); err != nil {
			newMaps = append(newMaps, newMap)
		} else {
			return newMaps, err
		}

	}
	return
}

func TestQueryAccount(t *testing.T) {
	testCases := map[string]accountCase{}
	router := gin.Default()
	SetupRouter(router)
	recorder := httptest.NewRecorder()

	//# Test cases #//
	testCases["query by name with query parameter"] = accountCase{
		method: http.MethodGet,
		url:    MajorGroupPath + AccountGroupPath + "?q=account_1",
		status: http.StatusOK,
		want:   []model.Account{{ID: 1, Name: "account_1"}},
	}
	testCases["query by id with route parameter"] = accountCase{
		method: http.MethodGet,
		url:    MajorGroupPath + AccountGroupPath + "/1",
		status: http.StatusOK,
		want:   model.Account{ID: 1, Name: "account_1"},
	}
	testCases["query with an not exist id"] = accountCase{
		method: http.MethodGet,
		url:    MajorGroupPath + AccountGroupPath + "/4",
		status: http.StatusOK,
		want:   httputility.HTTPError{Code: http.StatusNotFound, Message: model.ErrNoRow.Error()},
	}

	//# Testing  all cases #//
	for name, test := range testCases {
		t.Run(name, func(t *testing.T) {
			req, _ := http.NewRequest(test.method, test.url, nil)
			router.ServeHTTP(recorder, req)
			if !reflect.DeepEqual(recorder.Code, test.status) {
				t.Errorf("Status error: Got:%#v Want:%#v\n", recorder.Code, test.status)
			}

			result, _ := ioutil.ReadAll(recorder.Body)
			//var response model.Account
			var response interface{}

			err := json.Unmarshal(result, &response)

			fmt.Printf("response content is : %#v\n", response)
			if err != nil {
				fmt.Printf("response content is wrong: %#v\n", result)
			}

			switch response.(type) {
			case []interface{}:

				switch interfaces := test.want.(type) { //convert the want as a interface slice. It seems strange.
				case []interface{}:
					want, _ := SliceToMaps(interfaces)
					if !reflect.DeepEqual(response, want) {
						t.Errorf("Got:%#v Want:%#v\n", response, want)
					}
				}

			case interface{}:
				want, _ := StructToMap(test.want)
				if !reflect.DeepEqual(response, want) {
					t.Errorf("Got:%#v Want:%#v\n", response, want)
				}

			}

		})
	}
}
