package controller

import (
	"errors"
	"fmt"
	"net/http"

	"myAPI/httputility"

	"myAPI/model"

	"github.com/gin-gonic/gin"
)

// Auth godoc
// @Summary Auth admin
// @Description get admin info
// @Tags accounts,admin
// @Accept  json
// @Produce  json
// @Success 200 {object} model.Admin
// @Failure 400 {object} httputility.HTTPError
// @Failure 401 {object} httputility.HTTPError
// @Failure 404 {object} httputility.HTTPError
// @Failure 500 {object} httputility.HTTPError
// @Security ApiKeyAuth
// @Router /admin/auth [post]
func (c *Controller) Auth(ctx *gin.Context) {
	authHeader := ctx.GetHeader("Authorization")
	if len(authHeader) == 0 {
		httputility.NewError(ctx, http.StatusBadRequest, errors.New("please set Header Authorization"))
		return
	}
	if authHeader != "admin" {
		httputility.NewError(ctx, http.StatusUnauthorized, fmt.Errorf("this user isn't authorized to operation key=%s expected=admin", authHeader))
		return
	}
	admin := model.Admin{
		ID:   1,
		Name: "admin",
	}
	ctx.JSON(http.StatusOK, admin)
}
