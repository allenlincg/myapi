# Abstract
 <p>This is a RESTfull API with API document example for Golang with Swagger and air. It also uses Go modules and testing.</p>
 <p>The example clones from [celler](https://github.com/swaggo/swag/tree/master/example/celler) of swaggo example.</p>
 
# Requirements
* Linux or Windows
* make
* [GVM](https://github.com/moovweb/gvm) for Golang versiom management on Linux
    * Install trouble shotting - [curl: (1) Protocol "https" not supported or disabled in libcurl](https://www.796t.com/article.php?id=244412)
* [g](https://github.com/voidint/g) for Golang version managment on Windows
* GoLang
* [Swaggo](https://github.com/swaggo/swag)
* [Air](https://github.com/cosmtrek/air) for Live reload Go APPs (You can use it for developing without restart the service after you change your code.)
    * It can't be used on WSL.
    * You can try this example without Air for Windows
* [Docker](https://docs.docker.com/engine/install/ubuntu/) and [Docker-Compose](https://docs.docker.com/compose/install/) for using Air

# Usage
  You can use following steps to try it after you install the [Requirements](#requirements). Please install go V1.16.6  by go version managment tool.

* for Linux
```cmd
$ gvm install go1.16.6
```

* for windows
```cmd
$ g install 1.16.6
```

## Without Air
<p>You can run the APIs with swagger service by following steps.</p>

### for Linux
```cmd
$ gvm use go1.16.6
$ make init
$ make build
$ make run
```

### for windows
You must instal [g](https://github.com/voidint/g) and [make](https://community.chocolatey.org/packages/make/3.81) first. Then you can run following commands under Powershell.
```cmd
> g use 1.16.6
> make init
> make build
> make run
```
You can try the API by swagger by Web browser with http://localhost:8080/swagger


## Using Air for develop on Linux
You just need to launch the air service with docker container
```cmd
$ docker-compose up
$ gvm use go1.16.6
$ make init
$ make build
$ make run
```

## Run testing for develp
* run all unit tests
```cmd
make test
```

* check the test coverage
```cmd
make coverage
```

<p>You can read the Makefile to know more usages. </p>


## How to use the example
### using go modules
* create the go.mod under the root folder of project
```cmd
$ go mod init myAPI
```
* import the private package with the module name - myAPI, e.g.
```code
import (
    "myAPI/controller"
)
```


### creating a API
* ceating a RESTfull API for accounts
    * creat a data model account.go inside medel folder
    * creat a controller accounts.go inside controller folder
        * create REST API with Controller interface
    * add a group - accounts inside SetupRouter() in SetupRouter.go for each methods


### making a document for swagger
* setting the basic information for swagger service in main.go
```code 
	docs.SwaggerInfo.Title = "Swagger Example API 1"
	docs.SwaggerInfo.Description = "This is a sample server Petstore server."
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "localhost:8080"
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}
```

* setting the router for swagger by SetupSwaggerRouter() in setupRouter.go
* writing the [API operator](https://github.com/swaggo/swag) of swagger for an API - ListAccounts()
```code
// @Summary List accounts
// @Description get accounts
// @Tags accounts
// @Accept  json
// @Produce  json
// @Param q query string false "name search by q" Format(email)
// @Success 200 {array} model.Account
// @Failure 400 {object} httputility.HTTPError
// @Failure 404 {object} httputility.HTTPError
// @Failure 500 {object} httputility.HTTPError
// @Router /accounts [get]
```
    
    
### ceating an unit test for account API
* create an unit test file - accounts_test.go for accouts.go in controller folder
* create test case struct - accountCase
* create test function TestShowAccount()  for ShowAccount()
